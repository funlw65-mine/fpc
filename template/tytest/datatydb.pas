
unit datatydb;
interface

{
  Automatically converted by H2Pas 1.0.0 from datatydb.h
  The following command line parameters were used:
    datatydb.h
    -o
    datatydb.pas
}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


  {---------- headerfile for datatydb.ddl ---------- }
  { alignment is 8  }
  {---------- structures ---------- }
  { size 180  }

  type
    people = record
        name : array[0..69] of char;
        birth : array[0..9] of char;
        location : array[0..99] of char;
      end;

  {---------- record names ---------- }

  const
    PEOPLE_tbl = 1000;    
  {---------- field names ---------- }
    NAME_fld = 1001;    
    BIRTH_fld = 1002;    
    LOCATION_fld = 1003;    
  {---------- key names ---------- }
  {---------- sequence names ---------- }
  {---------- integer constants ---------- }

implementation


end.
