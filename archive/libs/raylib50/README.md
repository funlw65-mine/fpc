RAYLIB 5.1-dev - RAYGUI 4.1-dev
You have the following subfolders:

opengl11
opengl21
opengl33
opengl43

They contain the same library "libraylib.a" but wich was built against
the OpenGL version that is inserted in the name of the folder.

If you want that your app compile and run under a certain OpenGL version, copy
the library from that subfolder into fpc/libs main folder.