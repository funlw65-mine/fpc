RAYLIB 4.2 - RAYGUI 3.2
You have the following subfolders:

opengl11
opengl21
opengl33

They contain the same library "libraylib.a" but wich was built against
the OpenGL version that is inserted in the name of the folder.

If you want that your app compile and run under a certain OpenGL version, copy
the library from that subfolder into fpc/libs main folder.